// app.js
angular.module('sortApp', [])

.controller('mainController', function($scope, $http) {
  $scope.sortType     = 'name'; // set the default sort type
  $scope.sortReverse  = false;  // set the default sort order
  $scope.searchEmployee   = '';     // set the default search/filter term
  
  // create the list of sushi rolls 
   /*[
    { name: 'Cali Roll', fish: 'Crab', tastiness: 2 },
    { name: 'Philly', fish: 'Tuna', tastiness: 4 },
    { name: 'Tiger', fish: 'Eel', tastiness: 7 },
    { name: 'Rainbow', fish: 'Variety', tastiness: 6 }
  ];*/
  
  $http({
  method: 'GET',
  url: './data.json'
}).then(function successCallback(response) {
    // this callback will be called asynchronously
    // when the response is available
	$scope.empList = response.data;
  }, function errorCallback(response) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
});